let collection = [];
// Write the queue functions below.

let print = () => {
	return collection;
}

let enqueue = (item) => {
	collection.push(item);
	return collection;
}

let dequeue = () => {
	collection.shift();
	return collection;
}

let front = () =>{
	return collection[0];
}

let size = () =>{
	return collection.length;
}

let isEmpty = () =>{
	return (collection.length === 0);
}





module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};